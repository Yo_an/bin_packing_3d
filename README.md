# Implémentation

## Algorithme
L'algorithme insère chaque paquet dans le premie emplacement disponible - lorsque c'est possible.

A ce stade : 
- seule la disposition initiale du paquet est testée (3 possibles)

## Exécution du programme
bin fichier_des_conteneurs n l w h
- n : nombre conteneurs vide
- l, w, h : respectivement lenght, width, height des conteneurs
Soit n conteneurs de mêmes dimensions.

Exemple:
./bin/run "data/full_containers.txt" 1 6 6 6

## Fait
- EmptyContainer représente les conteneurs de stockage, FullContainer représente les objets à stocker.

## A corriger


## A rajouter
- Un tri par volume des conteneurs de stockage, ainsi qu'un autre pour ceux à stocker (règles à définir).
- Tester le stockage dans les trois positions possibles de chaque objet lorsque nécessaire.

