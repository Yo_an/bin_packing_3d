#ifndef EMPTY_CONTAINER_H
#define EMPTY_CONTAINER_H

#include <memory>

#include "Container.h"
#include "FullContainer.h"

class EmptyContainer : public Container {
private:
    std::shared_ptr<FullContainer> full_;
    std::shared_ptr<EmptyContainer> left_, right_, top_;

public:
    EmptyContainer();
    EmptyContainer(int length, int width, int height);
    EmptyContainer(const EmptyContainer &other);
    ~EmptyContainer() = default;

    /**
     * Insère un conteneur plein et crée les nouveaux espaces vides.
     * Le conteneur est toujours considéré au fond à gauche.
     * Cette version ne gère pas les espaces croisés.
    */
    bool setFull(std::shared_ptr<FullContainer> full);
    std::shared_ptr<EmptyContainer> makeSharedEmptyContainer(int lenght, int width, int height);

    int nbrFullContainers();
    int totalContainersVolume();

    std::list<std::shared_ptr<EmptyContainer>> getEmptyContainersSons();
    std::shared_ptr<FullContainer> getFull();

    void clear();
    void setOwnedFullContainerToStacked();

    EmptyContainer& operator=(const EmptyContainer&) = default;
    EmptyContainer(EmptyContainer&&) = default;
    EmptyContainer& operator=(EmptyContainer&&) = default;
};

#endif