#ifndef CONTAINER_H
#define CONTAINER_H

#include <iostream>

class Container {
protected:
    int length_, width_, height_;

public:
    Container();
    Container(int lenght, int width, int height);
    Container(const Container &other);
    ~Container() = default;

    int getLength();
    int getWidth();
    int getHeight();

    int volume();

    bool operator<=(const Container &other);
    
    friend std::ostream& operator<<(std::ostream& os, const Container& obj);
};

#endif