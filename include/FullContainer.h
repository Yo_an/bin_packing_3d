#ifndef FULL_CONTAINER_H
#define FULL_CONTAINER_H

#include "Container.h"

class FullContainer : public Container {
private:
    bool stacked_;

public:
    FullContainer();
    FullContainer(int length, int width, int height);
    FullContainer(const FullContainer &other);
    ~FullContainer() = default;

    void setStacked(bool value);
    bool getStacked();

    FullContainer& operator=(const FullContainer&) = default;
    FullContainer(FullContainer&&) = default;
    FullContainer& operator=(FullContainer&&) = default;
};

#endif