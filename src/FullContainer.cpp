#include "../include/FullContainer.h"

FullContainer::FullContainer() : Container(), stacked_(false) {}

FullContainer::FullContainer(int length, int width, int height) : Container(length, width, height), stacked_(false) {}

FullContainer::FullContainer(const FullContainer &other) : Container(other), stacked_(other.stacked_) {}

void FullContainer::setStacked(bool value) { stacked_ = value; }

bool FullContainer::getStacked() { return stacked_; }