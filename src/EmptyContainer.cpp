#include <memory>
#include <list>
#include <iostream>

#include "../include/EmptyContainer.h"

EmptyContainer::EmptyContainer() : Container(), full_(nullptr), left_(nullptr), right_(nullptr), top_(nullptr) {}

EmptyContainer::EmptyContainer(int length, int width, int height) : Container(length, width, height), full_(nullptr), left_(nullptr), right_(nullptr), top_(nullptr) {}

/**
 * Insère un conteneur plein et crée les nouveaux espaces vides.
 * Le conteneur est toujours considéré au fond à gauche. Créant 3 espaces vides : à gauche (devant), à droite, et au-dessus.
 * Cette version ne gère pas les espaces croisés.
*/
bool EmptyContainer::setFull(std::shared_ptr<FullContainer> full) {     
    if (!(*full <= *this)) { return false; }  

    full_ = full;
    full_->setStacked(true);

    left_ = makeSharedEmptyContainer(
        length_-full->getLength(), 
        full->getWidth(),
        height_
    );

    right_ = makeSharedEmptyContainer(
        length_,
        width_-full->getWidth(), 
        height_
    );

    top_ = makeSharedEmptyContainer(
        full->getLength(), 
        full->getWidth(), 
        height_-full->getHeight()
    );

    return true;
}

std::shared_ptr<FullContainer> EmptyContainer::getFull() { return full_; }

void EmptyContainer::clear() {
    if (full_) { 
        full_->setStacked(false);
        full_ = nullptr;
    }

    for (std::shared_ptr<EmptyContainer> ptr : {left_, right_, top_}) {
        if (ptr) {
            ptr->clear(); // UTILE ???
            ptr = nullptr;
        }
    }
}

void EmptyContainer::setOwnedFullContainerToStacked() {
    if (full_) { full_->setStacked(true); }
    for (std::shared_ptr<EmptyContainer> ptr : {left_, right_, top_}) {
        if (ptr) {
            ptr->setOwnedFullContainerToStacked();
        }
    }
}

int EmptyContainer::nbrFullContainers() {
    if (!full_) { return 0; }
    return 1 + left_->nbrFullContainers() + right_->nbrFullContainers() + top_->nbrFullContainers();
}

int EmptyContainer::totalContainersVolume() {
    if (!full_) { return 0; }
    return full_->volume() + 
            (left_ ? left_->totalContainersVolume() : 0) + 
            (right_ ? right_->totalContainersVolume() : 0) + 
            (top_ ? top_->totalContainersVolume() : 0);
}

std::list<std::shared_ptr<EmptyContainer>> EmptyContainer::getEmptyContainersSons() {
    return std::list<std::shared_ptr<EmptyContainer>>({left_, right_, top_});
}

std::shared_ptr<EmptyContainer> EmptyContainer::makeSharedEmptyContainer(int lenght, int width, int height) {
    return std::make_shared<EmptyContainer>(lenght, width, height);
}

EmptyContainer::EmptyContainer(const EmptyContainer &other) : Container(other) {
    full_ = other.full_ ? other.full_ : nullptr;
    left_ = other.left_ ? std::make_shared<EmptyContainer>(*(other.left_)) : nullptr;
    right_ = other.right_ ? std::make_shared<EmptyContainer>(*(other.right_)) : nullptr;
    top_ = other.top_ ? std::make_shared<EmptyContainer>(*(other.top_)) : nullptr;
}