#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>

#include "../include/EmptyContainer.h"
#include "../include/FullContainer.h"

/**
 * Fichier dans lequel chaque ligne représente un Container : length,width,height
 * Exemple : 
 *      3,3,3
 *      2,1,4
 *      ...
*/
template<typename ContainerType>
std::vector<std::shared_ptr<ContainerType>> fillContainerFromFile(const std::string &filename) {
    std::vector<std::shared_ptr<ContainerType>> containers;
    std::ifstream file(filename);

    if (!file) {
        std::cout << "Fichier introuvable !\n";
        return containers;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        int length, width, height;
        
        char delimiter = ',';
        if (!(iss >> length >> delimiter >> width >> delimiter >> height)) {
            std::cout << "Erreur de formatage d'une ligne : " << line << '\n';
        }
        containers.push_back(std::make_shared<ContainerType>(length, width, height));  
    }

    return containers;
}

int main(int argc, char* argv[]) {
    std::string filenameFullContainers;

    std::vector<std::shared_ptr<FullContainer>> fullContainers;
    std::vector<std::shared_ptr<EmptyContainer>> emptyContainers;
    
    // Nom fichier de conteneurs pleins + nom fichier de conteneurs vides disponibles.
    if (argc == 3) {
        std::string filenameEmptyContainers;

        filenameFullContainers = argv[1];
        filenameEmptyContainers = argv[2];

        fullContainers = fillContainerFromFile<FullContainer>(filenameFullContainers);
        emptyContainers = fillContainerFromFile<EmptyContainer>(filenameEmptyContainers);

    // Nom fichier de conteneurs pleins + (nombreConteneursVides length width height)
    } else if (argc == 6) {
        int n, l, w, h;

        filenameFullContainers = argv[1];

        n = std::atoi(argv[2]);
        l = std::atoi(argv[3]);
        w = std::atoi(argv[4]);
        h = std::atoi(argv[5]);

        fullContainers = fillContainerFromFile<FullContainer>(filenameFullContainers);
        
        for (int i = 0; i < n; ++i) {
            emptyContainers.push_back(std::make_shared<EmptyContainer>(l, w, h));
        }
    }

    if (fullContainers.size() == 0) {
        std::cout << "Erreur : Pas de conteneur plein !\n";
        return 1;
    }

    if (emptyContainers.size() == 0) {
        std::cout << "Erreur : Pas de conteneur vide !\n";
        return 1;
    }

    // ___DEBUT BIN PACKING 3D___

    int lastStackedVolume = 0;
    int totalStackedVolume = 0;

    int lastContainersUsed = 0;
    int totalContainersUsed = 0;
    std::vector<std::unique_ptr<EmptyContainer>> backupBestStorage;
    do {
        // Initialisation des conteneurs vides disponibles.
        std::list<std::shared_ptr<EmptyContainer>> lastEmptyContainers;
        std::transform(emptyContainers.begin(), emptyContainers.end(), std::back_inserter(lastEmptyContainers), 
            [](const std::shared_ptr<EmptyContainer> &ptr_eC) {
                ptr_eC->clear();
                return ptr_eC;
            }
        );

        // Rangement des conteneurs pleins dans les conteneurs vides disponibles.
        // La priorité est le remplissement des conteneurs déjà partiellement remplis.
        for (std::shared_ptr<FullContainer> &fC_p : fullContainers) {
            for (std::list<std::shared_ptr<EmptyContainer>>::iterator eC_it = lastEmptyContainers.begin(); eC_it != lastEmptyContainers.end(); ++eC_it) {

                // Si le conteneur plein n'a pas été inséré alors on va au prochain conteneur vide disponible.
                if (!(*eC_it)->setFull(fC_p)) { continue; }

                // Le conteneur a été inséré alors on met à jour la liste des conteneurs disponibles.
                // Puis on passe au conteneur plein suivant.
                std::list<std::shared_ptr<EmptyContainer>> newEmptyContainers = (*eC_it)->getEmptyContainersSons();

                // Ajout des nouveaux sous-conteneurs vides disponibles et suppression de celui qui vient d'être utilisé.
                lastEmptyContainers.splice(eC_it, newEmptyContainers, newEmptyContainers.begin(), newEmptyContainers.end());
                lastEmptyContainers.erase(eC_it);
                break;
            }
        }
        
        totalStackedVolume = std::accumulate(emptyContainers.begin(), emptyContainers.end(), 0,
            [](int sum, const std::shared_ptr<EmptyContainer> &ptr_eC) { 
                return sum + ptr_eC->totalContainersVolume();
            }
        );

        totalContainersUsed = std::accumulate(emptyContainers.begin(), emptyContainers.end(), 0,
            [] (int sum, const std::shared_ptr<EmptyContainer> &ptr_eC) {
                return sum + (ptr_eC->getFull() ? 1 : 0);
            }
        );

        // Sauvegarde du résultat pouvant stocker le plus de volume en un minimum de conteneurs vides.
        if (totalStackedVolume > lastStackedVolume || (totalStackedVolume == lastStackedVolume && totalContainersUsed != 0 && totalContainersUsed < lastContainersUsed)) {
            lastStackedVolume = totalStackedVolume;
            lastContainersUsed = totalContainersUsed;

            backupBestStorage.clear();       
            std::transform(emptyContainers.begin(), emptyContainers.end(), std::back_inserter(backupBestStorage),
                [](const std::shared_ptr<EmptyContainer> &ptr_eC) {
                    return std::make_unique<EmptyContainer>(*ptr_eC);
                }
            );
        }
        
    } while (std::next_permutation(fullContainers.begin(), fullContainers.end()));

    // ___FIN BIN PACKING 3D___ 


    // ___DEBUT RESULTATS___
    int nbrStackedContainers = 0;

    for (const std::shared_ptr<FullContainer> &ptr_fC : fullContainers) {
        ptr_fC->setStacked(false);
    }

    for (const std::unique_ptr<EmptyContainer> &eCI : backupBestStorage) {
        eCI->setOwnedFullContainerToStacked();
        nbrStackedContainers += eCI->nbrFullContainers();
    }

    std::cout << "Nombre de conteneurs pleins rangés : " << nbrStackedContainers << "/" << fullContainers.size() << '\n';
    std::cout << "Nombre de conteneurs initiaux utilisés : " << lastContainersUsed << "/" << emptyContainers.size() << "\n";
    std::cout << "Volume de conteneurs pleins stocké : " << lastStackedVolume << '\n';

    for (auto &fC_p : fullContainers) {
        std::cout << *fC_p << " --> " << (fC_p->getStacked() ? "Stocké" : "Non-stocké") << '\n';
    }

    // ___FIN RESULTATS___
    return 0;
}