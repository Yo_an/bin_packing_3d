#include <iostream>

#include "../include/Container.h"

Container::Container() {}
Container::Container(int length, int width, int height) : length_(length), width_(width), height_(height) {}
Container::Container(const Container &other) : length_(other.length_), width_(other.width_), height_(other.height_) {}


int Container::getLength() { return length_; }
int Container::getWidth() { return width_; }
int Container::getHeight() { return height_; }

int Container::volume() {
    return length_ * width_ * height_;
}

bool Container::operator<=(const Container &other) {
    return (length_ <= other.length_)
            && (width_ <= other.width_)
            && (height_ <= other.height_);
}

std::ostream& operator<<(std::ostream& os, const Container& obj) {
    os << "Container | l: " << obj.length_ << ", w: " << obj.width_ << ", h: " << obj.height_;
    return os;
}